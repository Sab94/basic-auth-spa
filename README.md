# Basic auth SPA

## Getting Started
* `npm install`
* `cp src/environments/privates.ts.example src/environments/privates.ts`
* `ng serve`
* open browser and go to `http://localhost:4200` 

## Directory Structure

* src/ - Where all your application code lives.
* src/assets/ - Public assets 
* src/app/pages/ - Where all pages live
* src/app/services/ - Helper services like the ApiService and UserService

