import { InjectionToken } from '@angular/core';
import { privates } from './privates';
import { EnvInterface } from './environment.interface';

export const specific = {
  production: true
};

const env = Object.assign(privates, specific);

export const environment: EnvInterface = env;

export let ENV = new InjectionToken< EnvInterface >( 'environment' );
