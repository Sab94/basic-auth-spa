export interface EnvInterface {
  app_url: string;
  api_url: string;
  production: boolean;
}
