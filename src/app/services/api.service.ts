import {Injectable, Inject} from '@angular/core';
import {Router} from '@angular/router';

import {environment as env, ENV} from '../../environments/environment';

import {Observable} from 'rxjs';

import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient,
              private router: Router,
              @Inject(ENV) private ENV) {
  }

  generateHeaders() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    return httpOptions;
  }

  get(url, data?) {
    if (typeof(data) === 'object') {
      url += '?';
      let i = 1;
      for (let k in data) {
        url += k + '=' + data[k];
        if (i < Object.keys(data).length) {
          url += '&';
        }
        i++;
      }
    }

    return this.http.get(this.ENV.api_url + url, this.generateHeaders());

  }

  post(url, data) {
    return this.http.post(this.ENV.api_url + url, data, this.generateHeaders());
  }

  handleError(res: Response): Observable<any> {
    const data = res;

    let showPopup = true;

    return Observable.throw(data || 'Server Error');
  }

}
