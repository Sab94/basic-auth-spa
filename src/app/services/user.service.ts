import {Injectable, Output, EventEmitter} from '@angular/core';
import {ApiService} from './api.service';

@Injectable()
export class UserService {

  constructor(
    private api: ApiService
  ) {}

  public login(user) {
    return new Promise(function (resolve, reject) {
      this.api.post('login', user)
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    }.bind(this));
  }

  public register(user) {
    return new Promise(function (resolve, reject) {
      this.api.post('register', user)
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    }.bind(this));
  }

  public verify(code) {
    return new Promise(function (resolve, reject) {
      this.api.get('register/verify/' + code)
          .subscribe(
              res => {
                resolve(res);
              },
              err => {
                reject(err);
              }
          );
    }.bind(this));
  }
}
