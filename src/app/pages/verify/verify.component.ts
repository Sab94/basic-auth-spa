import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {

  loading = true;
  user: any;
  constructor(private activatedRoute: ActivatedRoute,
              private userService: UserService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['confirmation']) {
        this.userService.verify(params['confirmation']).then( res => {
          this.user = res['user'];
          this.loading = false;
        }).catch( result => {
          this.loading = false;
        });
      }
    });
  }

}
