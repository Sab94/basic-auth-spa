import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formErrors: any;
  singleError: string;
  formLoading: boolean;

  user = {
    email: '',
    password: ''
  };
  callbackUser: any;
  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  performLogin() {
    this.formLoading = true;
    this.userService.login(this.user)
      .then(result => {
        this.callbackUser = result['user'];
        this.formErrors = null;
        this.singleError = null;
      })
      .catch(result => {
        if (typeof result.error.error === 'string') {
          this.formErrors = null;
          this.singleError = result.error.error;
        } else {
          this.formErrors = result.error.error;
        }
      }).then((result: any) => this.formLoading = false);
  }
}
