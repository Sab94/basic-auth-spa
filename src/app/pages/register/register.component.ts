import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  formErrors: any;
  singleError: string;
  formLoading: boolean;

  user = {
    email: '',
    password: '',
    name: ''
  };

  callbackUser: any;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  performRegistration() {
    this.formLoading = true;
    this.userService.register(this.user)
        .then(result => {
          this.callbackUser = result['user'];
          this.formErrors = null;
          this.singleError = null;
        })
        .catch(result => {
          if (typeof result.error.error === 'string') {
            this.formErrors = null;
            this.singleError = result.error.error;
          } else {
            this.formErrors = result.error.error;
          }
        }).then((result: any) => this.formLoading = false);
  }

}
